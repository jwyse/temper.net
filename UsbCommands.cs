﻿namespace temper.net
{
    public static class UsbCommands
    {
        public static readonly byte[] ReadSensorId =
        {
            0x00,
            0x01,
            0x89,
            0x85,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00
        };

        public static readonly byte[] ReadTemper =
        {
            0x00,
            0x01,
            0x80,
            0x33,
            0x01,
            0x00,
            0x00,
            0x00,
            0x00
        };

        public const int ReadTemperSuccess = 128;

        public static readonly byte[] GetCalibration =
        {
            0x00,
            0x01,
            0x82,
            0x77,
            0x01,
            0x00,
            0x00,
            0x00,
            0x00
        };

        public static readonly byte[] GetVersion =
        {
            0x00,
            0x01,
            0x86,
            0xFF,
            0x01,
            0x00,
            0x00,
            0x00,
            0x00
        };

        public static readonly byte[] Stop =
        {
            0x00,
            0x01,
            0x88,
            0x55,
            0x00,
            0x00,
            0x00,
            0x00,
            0x00
        };

        // Moved from TEMPer_Setting.cs
        // What does this do??
        public static readonly byte[] pBuffer2 =
        {
            0x00,
            0x01,
            0x85,
            0xDD,
            0x01,
            0x01,
            0x00,
            0x00,
            0x00
        };
    }
}