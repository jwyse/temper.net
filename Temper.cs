﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace temper.net
{
    public class Temper : IDisposable
    {
        #region results

        public abstract class DeviceResult
        {
            public bool Result { get; set; }
            public Exception Exception { get; set; }

            public static string BytesToString(byte[] bytes, bool hex)
            {
                return string.Join(", ", bytes.Select(m => m.ToString(hex ? "X" : null)).ToArray());
            }
        }

        public class WriteResult : DeviceResult
        {
            public byte[] Command { get; set; }
            public ulong BytesWritten;

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.AppendLine($"Write result: {Result}");
                sb.AppendLine($"Command: {BytesToString(Command, false)}");
                sb.AppendLine($"Bytes written: {BytesWritten}");
                return sb.ToString();
            }
        }

        public class ReadResult : DeviceResult
        {
            public byte[] Buffer { get; set; }
            public ulong BytesRead;

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.AppendLine($"Read result: {Result}");
                sb.AppendLine($"Buffer: {BytesToString(Buffer, false)}");
                sb.AppendLine($"Bytes read: {BytesRead}");
                return sb.ToString();
            }
        }

        public class ErrorMessageResult : DeviceResult
        {
            public uint ResultCode { get; set; }
            public string[] Messages = new string[10];
            public uint MessageSize { get; set; }

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.AppendLine($"GetErrorMsg result: {Result}");
                sb.AppendLine($"Error message size: {MessageSize}");
                sb.AppendLine($"Messages: ");
                foreach (var m in Messages)
                    sb.AppendLine(m);
                return sb.ToString();
            }
        }

        #endregion results

        public const int VendorId = 3141;
        public const int TEMPer2ProductId = 29697;

        /// <summary>
        /// Bytes to read
        /// </summary>
        private const ushort InputLength = 9;
        /// <summary>
        /// Bytes to write
        /// </summary>
        private const ushort OutputLength = 9;

        private IntPtr _device;
        private readonly int _vendorId;
        private readonly int _productId;
        private ulong _bytesWritten;
        private ulong _bytesRead;

        private DateTime _nextOpAvailable;

        public Temper() : this(VendorId, TEMPer2ProductId) { }
        public Temper(int vendorId, int productId)
        {
            _vendorId = vendorId;
            _productId = productId;
            _nextOpAvailable = DateTime.Now;
            Open();
            Debug.Assert(IsOpen, "Device should be open.");
            Debug.Assert(RDingUSBNet.GetInputLength(_device) == InputLength);
            Debug.Assert(RDingUSBNet.GetOutputLength(_device) == OutputLength);
        }

        private bool IsOpen => _device.ToInt32() != 0 && _device.ToInt32() != -1;

        private void CheckOpen()
        {
            if (!IsOpen)
            {
                Close();
                Open();
            }
        }

        public WriteResult WriteDevice(byte[] command)
        {
            CheckOpen();
            var wr = new WriteResult { Command = command };
            try
            {
                Throttle();
                wr.Result = RDingUSBNet.WriteUSB(_device, command, OutputLength, ref _bytesWritten);
                SetNextOp();
                wr.BytesWritten = _bytesWritten;
            }
            catch (Exception ex)
            {
                wr.Exception = ex;
            }
            return wr;
        }

        public ReadResult ReadDevice()
        {
            CheckOpen();
            var rr = new ReadResult
            {
                Buffer = new byte[9],
            };
            try
            {
                var buffer = new byte[9];
                Throttle();
                rr.Result = RDingUSBNet.ReadUSB(_device, buffer, InputLength, ref _bytesRead);
                SetNextOp();
                rr.BytesRead = _bytesRead;
                rr.Buffer = buffer;
            }
            catch (Exception ex)
            {
                rr.Exception = ex;
            }
            return rr;
        }

        public ErrorMessageResult GetErrorMessage()
        {
            CheckOpen();
            var er = new ErrorMessageResult { MessageSize = 10 }; // ??
            Throttle();
            er.ResultCode = RDingUSBNet.GetErrorMsg(ref er.Messages, er.MessageSize);
            SetNextOp();
            return er;
        }

        public static string GetFirmwareVersion()
        {
            using (var dev = new Temper())
            {
                return dev.GetFirmwareVersionInternal();
            }
        }

        public static double[] GetCalibration()
        {
            using (var dev = new Temper())
            {
                var wr = dev.WriteDevice(UsbCommands.GetCalibration);
                if (!wr.Result)
                {
                    throw new ApplicationException("Write failed: GetCalibration");
                }

                var r = dev.ReadDevice();
                if (!r.Result)
                {
                    throw new ApplicationException("Read failed.");
                }
                if (r.Buffer[1] != 130)
                {
                    throw new ApplicationException("Buffer[1] != 130");
                }

                return new[]
                {
                    Tools.ConvertCalibrationResult(r.Buffer[3]), Tools.ConvertCalibrationResult(r.Buffer[4])
                };
            }
        }

        private string GetFirmwareVersionInternal()
        {
            var wr = WriteDevice(UsbCommands.GetVersion);
            if (!wr.Result)
            {
                throw new InvalidOperationException("Write failed: GetVersion command");
            }

            var r1 = ReadDevice();
            if (!r1.Result) throw new InvalidOperationException("Read failed.");

            var r2 = ReadDevice();
            if (!r2.Result) throw new InvalidOperationException("Read failed.");

            return Encoding.ASCII.GetString(r1.Buffer, 1, 8) + Encoding.ASCII.GetString(r2.Buffer, 1, 8);
        }

        private bool Open()
        {
            _device = RDingUSBNet.OpenUSBDevice(_vendorId, _productId);
            SetNextOp();
            return _device.ToInt32() != -1;
        }

        private void Close()
        {
            try
            {
                _device = RDingUSBNet.OpenUSBDevice(_vendorId, _productId);
                if (_device.ToInt32() != -1)
                {
                    RDingUSBNet.WriteUSB(_device, UsbCommands.Stop, OutputLength, ref _bytesWritten);
                    RDingUSBNet.CloseUSBDevice(_device);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Dispose()
        {
            Close();
        }

        private void SetNextOp()
        {
            _nextOpAvailable = DateTime.Now.AddMilliseconds(100);
            if (RDingUSBNet.GetInputLength(_device) != InputLength) throw new InvalidOperationException("Input lengths differ!");
            if (RDingUSBNet.GetOutputLength(_device) != OutputLength) throw new InvalidOperationException("Output lengths differ!");
        }

        private void Throttle()
        {
            while (DateTime.Now < _nextOpAvailable)
            { }
        }
    }
}
