﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace temper.net
{
    public static class Tools
    {
        public static double ConvertCalibrationResult(byte b)
        {
            if (b <= 127)
                return b / 16.0;
            return -(256 - b) / 16.0;
        }

        public static double CtoF(this double c)
        {
            return c * 1.8 + 32.0;
        }

        public static double FtoC(this double f)
        {
            return (f - 32) / 1.8;
        }

        public static double ReadTemperature(byte hi, byte lo)
        {
            // if first bit of hi is 1, then temp is negative
            if ((hi & 128) == 128)
            {
                return -((256 - hi) + (1 + (~lo >> 4)) / 16.0);
            }
            return hi + ((lo >> 4) / 16.0);
        }
    }
}
